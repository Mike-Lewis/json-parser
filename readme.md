# JSON Parser

## Getting started
Ensure you have Node, NPM installed globally before proceeding. Tested with Node version 8+

```
npm install
npm start
```

Linting:
```
npm run lint
```

## Testing

For the sake of the allowed time I made the hard decision not to include tests!

### Assumptions

1. Students can only belong to 1 team
1. Teams are already sorted by ID
1. Will not be loading large JSON files into memory

### Improvements
1. TESTS!
1. Better error handling
1. Asynchronous file reads
1. Improve the addStudentsToTeams function so that the filteredStudents array does not need to iterate over on every team iteration
