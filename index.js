const { loadJSON, filterStudents, addStudentsToTeams, writeToJSON } = require('./lib/util');
const dir = require('path').join(__dirname);

/**
 * The main program function.
 */
async function executeProgram() {

  // store Student to Team relationship data
  const teamAllocation = await loadJSON(`${dir}/data/student-teams.json`);

  const filteredStudents = await loadJSON(`${dir}/data/students.json`)
    .then(students => {
      return filterStudents(students, teamAllocation);
    })
    .catch(err => {
      throw new Error(err);
    });

  const teams = await loadJSON(`${dir}/data/teams.json`)
    .then(teams => {
      return addStudentsToTeams(filteredStudents, teams);
    })
    .catch(err => {
      throw new Error(err);
    });
    
  writeToJSON(`${dir}/result.json`, { teams });
}

// Run the program
executeProgram();
