const fs = require('fs');

/**
 * Reads a file and returns as a parsed JSON object.
 * @param {string} path // the file location
 * @return {Object|null}
 */
const loadJSON = path =>
  new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(JSON.parse(data));
      }      
    });
  });

/**
 * Gets the team id from a matching student id.
 * @param {Object[]} teamAllocation // student to team relationship
 * @param {number} studentId
 * @return {number|null} // team id
 */
const getTeamId = (teamAllocation, studentId) => {
  const match = teamAllocation.find(item => item.studentId === studentId);
  if (match) {
    return match.teamId;
  }
  return null;
};

/**
 * Filters out injured students, concatenate names and adds their team id.
 * @param {Object[]} students // all students
 * @param {Object[]} teamAllocation // student to team relationship
 * @return {Object[]} // filtered students
 */
const filterStudents = (students, teamAllocation) => {
  return students
    .filter(student => !student.injured)
    .map(student => {
      const fullName = `${student.firstName} ${student.lastName}`;
      const teamId = getTeamId(teamAllocation, student.id);
      return { id: student.id, fullName, teamId };         
    })
    .sort((a, b) => a.fullName.localeCompare(b.fullName));
};

/**
 * Adds students into the teams array.
 * @param {Object[]} filteredStudents
 * @param {Object[]} teams
 * @return {Object[]} // teams with students array
 */
const addStudentsToTeams = ( filteredStudents, teams ) => {
  return teams.map(team => {
    const studentsInTeam = filteredStudents
      .filter(student => student.teamId === team.id)
      .map(student => {
        return {
          id: student.id,
          fullName: student.fullName
        };
      });
    return { ...team, students: studentsInTeam };
  });
};

/**
 * Write to a formatted json file from an object 
 * @param {string} path
 * @param {Object} content
 */
const writeToJSON = (path, content) => {
  const json = JSON.stringify(content, null, 2);
  try {
    const writeStream = fs.createWriteStream(path, { flags: 'w' });
    writeStream.on('finish', () => {
      console.log('write finished');
    });
    writeStream.write(json);
    writeStream.end();
  } catch (err) {
    console.error(err);
  }
};

module.exports = {
  loadJSON,
  writeToJSON,
  getTeamId,
  filterStudents,
  addStudentsToTeams,
};
